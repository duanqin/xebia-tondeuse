package com.xebia.domain;

import com.xebia.exception.TondeuseAppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class Position {

    private static final Logger LOGGER = LoggerFactory.getLogger(Position.class);

    private Orientation orientation;
    private Coordonnees coordonnees;

    public Position(Orientation orientation, Coordonnees coordonnees) {
        this.orientation = orientation;
        this.coordonnees = coordonnees;
    }

    public void turnToLeft() throws TondeuseAppException {
        Optional<Orientation> calculatedOrientation = Orientation.getOrientationFromDegre(orientation.getDegre() + 90);
        if (calculatedOrientation.isPresent()) {
            orientation = calculatedOrientation.get();
        } else {
            throw new TondeuseAppException("Can not turn left, actuel orientation is " + orientation);
        }
    }

    public void turnToRight() throws TondeuseAppException {
        Optional<Orientation> calculatedOrientation = Orientation.getOrientationFromDegre(orientation.getDegre() - 90);
        if (calculatedOrientation.isPresent()) {
            orientation = calculatedOrientation.get();
        } else {
            throw new TondeuseAppException("Can not turn left, actuel orientation is " + orientation);
        }
    }

    public void forward() {
        coordonnees = calculateNextCoordonnees();
    }

    public Coordonnees calculateNextCoordonnees() {
        Coordonnees newCoordonnees = Coordonnees.cloneCoordonnees(coordonnees);
        if (Orientation.E.equals(orientation)) {
            newCoordonnees.forwardX();
        } else if (Orientation.N.equals(orientation)) {
            newCoordonnees.forwardY();
        } else if (Orientation.W.equals(orientation)) {
            newCoordonnees.stepBackX();
        } else if (Orientation.S.equals(orientation)) {
            newCoordonnees.stepBackY();
        }
        return newCoordonnees;
    }

    public void printCurrentPosition() {
        LOGGER.info("{},{},{}",coordonnees.getX(),coordonnees.getY(),orientation);
    }
}
