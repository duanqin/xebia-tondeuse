package com.xebia.domain;

import com.xebia.exception.TondeuseAppException;

import java.util.List;

public class Tondeuse {


    private Position position;
    private Pelouse pelouse;

    public Tondeuse(Position initialPosition, Pelouse pelouse) {
        this.position = initialPosition;
        this.pelouse = pelouse;
    }

    private void takeAInstruction(Instruction instruction) throws TondeuseAppException {
        if (Instruction.STRAIGHT.equals(instruction)) {
            if (!willOutOfPelouseAfterInstruction()) {
                position.forward();
            }
        } else if (Instruction.LEFT.equals(instruction)) {
            position.turnToLeft();
        } else if (Instruction.RIGHT.equals(instruction))  {
            position.turnToRight();
        }
    }

    public void takeInstructions(List<Instruction> instructionList) throws TondeuseAppException{
        for (Instruction instruction : instructionList) {
            takeAInstruction(instruction);
        }
    }

    private boolean willOutOfPelouseAfterInstruction() {
        Coordonnees nextCoordonnees = position.calculateNextCoordonnees();
        return pelouse.isCoordonneesOutSide(nextCoordonnees);
    }

    public void getCurrentPosition() {
        position.printCurrentPosition();
    }

}
