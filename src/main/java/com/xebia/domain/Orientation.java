package com.xebia.domain;

import java.util.Arrays;
import java.util.Optional;

public enum Orientation {
    N(0),
    W(90),
    S(180),
    E(270);

    private int degre;

    Orientation(int degre) {
        this.degre = degre;
    }

    public int getDegre() {
        return degre;
    }

    public static Optional<Orientation> getOrientationFromDegre(final int degre) {
        int degre2 = (degre + 360) % 360;
        return Arrays.stream(values()).filter(orientation -> orientation.getDegre() == degre2).findFirst();
    }
}
