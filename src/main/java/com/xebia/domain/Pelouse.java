package com.xebia.domain;


import com.xebia.exception.TondeuseAppException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Pelouse {

    private static Coordonnees minCoordonnees;
    private static Coordonnees maxCoordonnees;

    private static Pelouse pelouse;

    public static Pelouse getInstance() throws TondeuseAppException {
        if (pelouse == null) {
            pelouse = buildAPelouse();
        }
        return pelouse;
    }

    private static Pelouse buildAPelouse() throws TondeuseAppException {
        Properties pelouseProperties = new Properties();
        InputStream in = null;
        try {
            in = Pelouse.class.getClassLoader().getResourceAsStream("pelouse.properties");

            pelouseProperties.load(in);
            Integer minX = Integer.parseInt(pelouseProperties.getProperty("min-x"));
            Integer minY = Integer.parseInt(pelouseProperties.getProperty("min-y"));
            Integer maxX = Integer.parseInt(pelouseProperties.getProperty("max-x"));
            Integer maxY = Integer.parseInt(pelouseProperties.getProperty("max-y"));

            Pelouse pelouse = new Pelouse();
            minCoordonnees = new Coordonnees(minX,minY);
            maxCoordonnees = new Coordonnees(maxX,maxY);
            return pelouse;
        } catch (IOException e) {
            throw new TondeuseAppException(e);
        }
    }

    public boolean isCoordonneesOutSide(Coordonnees nextCoordonnees) {
        return nextCoordonnees.getX() < minCoordonnees.getX()
                || nextCoordonnees.getY() < minCoordonnees.getY()
                || nextCoordonnees.getX() > maxCoordonnees.getX()
                || nextCoordonnees.getY() > maxCoordonnees.getY();
    }

    public static void setMaxCoordonnees(Coordonnees maxCoordonnees) {
        Pelouse.maxCoordonnees = maxCoordonnees;
    }
}
