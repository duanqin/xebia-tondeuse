package com.xebia.exception;

public class TondeuseAppException extends Exception{

    public TondeuseAppException(Throwable e){
        super(e.getMessage());
    }

    public TondeuseAppException(String message, Throwable cause){
        super(message, cause);
    }

    public TondeuseAppException(String message) {
        super(message);
    }
}
