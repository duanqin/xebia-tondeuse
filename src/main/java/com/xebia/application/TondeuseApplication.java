package com.xebia.application;

import com.xebia.domain.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TondeuseApplication {

    public static void main(String [ ] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(TondeuseApplication.class.getClassLoader().getResourceAsStream("inputFile.txt")));

        String pelouseParameter = br.readLine();
        String tondeuse1Parameter = br.readLine();
        String tondeuse1InstructionString = br.readLine();
        String tondeuse2Parameter = br.readLine();
        String tondeuse2InstructionString = br.readLine();

        /*
         * Pelouse
         */
        Integer maxCoordonneesX = Integer.parseInt(pelouseParameter.substring(0,1));
        Integer maxCoordonneesY = Integer.parseInt(pelouseParameter.substring(2,3));
        Pelouse pelouse = Pelouse.getInstance();
        pelouse.setMaxCoordonnees(new Coordonnees(maxCoordonneesX,maxCoordonneesY));



        /*
         *  Tondeuse1
         */
        Integer tondeuse1X = Integer.parseInt(tondeuse1Parameter.substring(0,1));
        Integer tondeuse1Y = Integer.parseInt(tondeuse1Parameter.substring(2,3));
        Orientation tondeuse1Orientation = Orientation.valueOf(tondeuse1Parameter.substring(4,5));

        Coordonnees tondeuseInitialeCoordonnees = new Coordonnees(tondeuse1X,tondeuse1Y);
        Position initialePosition = new Position(tondeuse1Orientation,tondeuseInitialeCoordonnees);

        Tondeuse tondeuse1 = new Tondeuse(initialePosition,pelouse);


        List<Instruction> tondeuse1InstructionList = new ArrayList<>();
        Arrays.stream(tondeuse1InstructionString.trim().split("")).forEach(a -> tondeuse1InstructionList.add(Instruction.getInstructionFromLettre(a).get()));
        tondeuse1.takeInstructions(tondeuse1InstructionList);
        tondeuse1.getCurrentPosition();

        /*
         *  Tondeuse2
         */
        Integer tondeuse2X = Integer.parseInt(tondeuse2Parameter.substring(0,1));
        Integer tondeuse2Y = Integer.parseInt(tondeuse2Parameter.substring(2,3));
        Orientation tondeuse2Orientation = Orientation.valueOf(tondeuse2Parameter.substring(4,5));
        Coordonnees tondeuseInitialeCoordonnees2 = new Coordonnees(tondeuse2X,tondeuse2Y);

        Position initialePosition2 = new Position(tondeuse2Orientation,tondeuseInitialeCoordonnees2);

        Tondeuse tondeuse2 = new Tondeuse(initialePosition2,pelouse);

        List<Instruction> tondeuse2InstructionList = new ArrayList<>();
        Arrays.stream(tondeuse2InstructionString.trim().split("")).forEach(a -> tondeuse2InstructionList.add(Instruction.getInstructionFromLettre(a).get()));
        tondeuse2.takeInstructions(tondeuse2InstructionList);
        tondeuse2.getCurrentPosition();
    }


}
